/* Copyright (C) 2019 Nemirtingas
 * This file is part of gamepad.
 *
 * gamepad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gamepad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gamepad.  If not, see <https://www.gnu.org/licenses/>
 */

#include "gamepad.h"

Gamepad::Gamepad() :
    id{ 0, 0 },
    up(false), down(false),
    left(false), right(false),
    start(false), back(false),
    left_shoulder(false), right_shoulder(false),
    left_thumb(false), right_thumb(false),
    a(false), b(false),
    x(false), y(false),
    guide(false),
    left_stick{ 0.0f, 0.0f }, right_stick{ 0.0f, 0.0f },
    left_trigger(0.0f), right_trigger(0.0f)
{}

Gamepad::~Gamepad()
{}

const std::map<gamepad_id_t, gamepad_type_t, gamepad_id_less_t> Gamepad::gamepads = {
    {{0x0079, 0x18d4}, {gamepad_type_t::type_e::Xbox360, "GPD Win 2 X-Box Controller"}},
    {{0x044f, 0xb326}, {gamepad_type_t::type_e::Xbox360, "Thrustmaster Gamepad GP XID"}},
    {{0x045e, 0x028e}, {gamepad_type_t::type_e::Xbox360, "Microsoft X-Box 360 pad"}},
    {{0x045e, 0x028f}, {gamepad_type_t::type_e::Xbox360, "Microsoft X-Box 360 pad v2"}},
    {{0x045e, 0x0291}, {gamepad_type_t::type_e::Xbox360, "Xbox 360 Wireless Receiver (XBOX)"}},
    {{0x045e, 0x02a0}, {gamepad_type_t::type_e::Xbox360, "Microsoft X-Box 360 Big Button IR"}},
    {{0x045e, 0x02a1}, {gamepad_type_t::type_e::Xbox360, "Microsoft X-Box 360 pad"}},
    {{0x045e, 0x02dd}, {gamepad_type_t::type_e::XboxOne, "Microsoft X-Box One pad"}},
    {{0x044f, 0xb326}, {gamepad_type_t::type_e::XboxOne, "Microsoft X-Box One pad (Firmware 2015)"}},
    {{0x045e, 0x02e0}, {gamepad_type_t::type_e::XboxOne, "Microsoft X-Box One S pad (Bluetooth)"}},
    {{0x045e, 0x02e3}, {gamepad_type_t::type_e::XboxOne, "Microsoft X-Box One Elite pad"}},
    {{0x045e, 0x02ea}, {gamepad_type_t::type_e::XboxOne, "Microsoft X-Box One S pad"}},
    {{0x045e, 0x02fd}, {gamepad_type_t::type_e::XboxOne, "Microsoft X-Box One S pad (Bluetooth)"}},
    {{0x045e, 0x02ff}, {gamepad_type_t::type_e::XboxOne, "Microsoft X-Box One Elite pad"}},
    {{0x045e, 0x0719}, {gamepad_type_t::type_e::Xbox360, "Xbox 360 Wireless Receiver"}},
    {{0x046d, 0xc21d}, {gamepad_type_t::type_e::Xbox360, "Logitech Gamepad F310"}},
    {{0x046d, 0xc21e}, {gamepad_type_t::type_e::Xbox360, "Logitech Gamepad F510"}},
    {{0x046d, 0xc21f}, {gamepad_type_t::type_e::Xbox360, "Logitech Gamepad F710"}},
    {{0x046d, 0xc242}, {gamepad_type_t::type_e::Xbox360, "Logitech Chillstream Controller"}},
    {{0x0f0d, 0x00c1}, {gamepad_type_t::type_e::Switch, "HORI Pad Switch"}},
    {{0x0f0d, 0x0092}, {gamepad_type_t::type_e::Switch, "HORI Pokken Tournament DX Pro Pad"}},
    {{0x0f0d, 0x00f6}, {gamepad_type_t::type_e::Switch, "HORI Wireless Switch Pad"}},
    {{0x0f0d, 0x00dc}, {gamepad_type_t::type_e::Switch, "HORI Battle Pad"}},
    {{0x20d6, 0xa711}, {gamepad_type_t::type_e::Switch, "PowerA Wired Controller Plus/PowerA Wired Gamcube Controller"}},
    {{0x0e6f, 0x0185}, {gamepad_type_t::type_e::Switch, "PDP Wired Fight Pad Pro for Nintendo Switch"}},
    {{0x0e6f, 0x0180}, {gamepad_type_t::type_e::Switch, "PDP Faceoff Wired Pro Controller for Nintendo Switch"}},
    {{0x0e6f, 0x0181}, {gamepad_type_t::type_e::Switch, "PDP Faceoff Deluxe Wired Pro Controller for Nintendo Switch"}},
    {{0x054c, 0x0268}, {gamepad_type_t::type_e::PS3, "Sony PS3 Controller"}},
    {{0x0925, 0x0005}, {gamepad_type_t::type_e::PS3, "Sony PS3 Controller"}},
    {{0x8888, 0x0308}, {gamepad_type_t::type_e::PS3, "Sony PS3 Controller"}},
    {{0x1a34, 0x0836}, {gamepad_type_t::type_e::PS3, "Afterglow PS3"}},
    {{0x0f0d, 0x006e}, {gamepad_type_t::type_e::PS3, "HORI horipad4 PS3"}},
    {{0x0f0d, 0x0066}, {gamepad_type_t::type_e::PS3, "HORI horipad4 PS4"}},
    {{0x0f0d, 0x005f}, {gamepad_type_t::type_e::PS3, "HORI Fighting commander PS3"}},
    {{0x0f0d, 0x005e}, {gamepad_type_t::type_e::PS3, "HORI Fighting commander PS4"}},
    {{0x0738, 0x8250}, {gamepad_type_t::type_e::PS3, "Madcats Fightpad Pro PS4"}},
    {{0x0079, 0x181a}, {gamepad_type_t::type_e::PS3, "Venom Arcade Stick"}},
    {{0x0079, 0x0006}, {gamepad_type_t::type_e::PS3, "PC Twin Shock Controller"}},
    {{0x2563, 0x0523}, {gamepad_type_t::type_e::PS3, "Digiflip GP006"}},
    {{0x11ff, 0x3331}, {gamepad_type_t::type_e::PS3, "SRXJ-PH2400"}},
    {{0x20bc, 0x5500}, {gamepad_type_t::type_e::PS3, "ShanWan PS3"}},
    {{0x044f, 0xb315}, {gamepad_type_t::type_e::PS3, "Firestorm Dual Analog 3"}},
    {{0x0f0d, 0x004d}, {gamepad_type_t::type_e::PS3, "Horipad 3"}},
    {{0x0f0d, 0x0009}, {gamepad_type_t::type_e::PS3, "HORI BDA GP1"}},
    {{0x0e8f, 0x0008}, {gamepad_type_t::type_e::PS3, "Green Asia"}},
    {{0x0f0d, 0x006a}, {gamepad_type_t::type_e::PS3, "Real Arcade Pro 4"}},
    {{0x0e6f, 0x011e}, {gamepad_type_t::type_e::PS3, "Rock Candy PS4"}},
    {{0x0e6f, 0x0214}, {gamepad_type_t::type_e::PS3, "Afterglow PS3"}},
    {{0x056e, 0x2013}, {gamepad_type_t::type_e::PS3, "JC-U4113SBK"}},
    {{0x0738, 0x8838}, {gamepad_type_t::type_e::PS3, "Madcatz Fightstick Pro"}},
    {{0x1a34, 0x0836}, {gamepad_type_t::type_e::PS3, "Afterglow PS3"}},
    {{0x0f30, 0x1100}, {gamepad_type_t::type_e::PS3, "Quanba Q1 fight stick"}},
    {{0x0f0d, 0x0087}, {gamepad_type_t::type_e::PS3, "HORI fighting mini stick"}},
    {{0x8380, 0x0003}, {gamepad_type_t::type_e::PS3, "BTP 2163"}},
    {{0x1345, 0x1000}, {gamepad_type_t::type_e::PS3, "PS2 ACME GA-D5"}},
    {{0x0e8f, 0x3075}, {gamepad_type_t::type_e::PS3, "SpeedLink Strike FX"}},
    {{0x0e6f, 0x0128}, {gamepad_type_t::type_e::PS3, "Rock Candy PS3"}},
    {{0x2c22, 0x2000}, {gamepad_type_t::type_e::PS3, "Quanba Drone"}},
    {{0x06a3, 0xf622}, {gamepad_type_t::type_e::PS3, "Cyborg V3"}},
    {{0x044f, 0xd007}, {gamepad_type_t::type_e::PS3, "Thrustmaster wireless 3-1"}},
    {{0x25f0, 0x83c3}, {gamepad_type_t::type_e::PS3, "Gioteck vx2"}},
    {{0x05b8, 0x1006}, {gamepad_type_t::type_e::PS3, "JC-U3412SBK"}},
    {{0x20d6, 0x576d}, {gamepad_type_t::type_e::PS3, "Power A PS3"}},
    {{0x0e6f, 0x1314}, {gamepad_type_t::type_e::PS3, "PDP Afterglow Wireless PS3 controller"}},
    {{0x0738, 0x3180}, {gamepad_type_t::type_e::PS3, "Mad Catz Alpha PS3 mode"}},
    {{0x0738, 0x8180}, {gamepad_type_t::type_e::PS3, "Mad Catz Alpha PS4 mode"}},
    {{0x0e6f, 0x0203}, {gamepad_type_t::type_e::PS3, "Victrix Pro FS"}},
    {{0x054c, 0x05c4}, {gamepad_type_t::type_e::PS4, "Sony PS4 Controller"}},
    {{0x054c, 0x09cc}, {gamepad_type_t::type_e::PS4, "Sony PS4 Slim Controller"}},
    {{0x054c, 0x0ba0}, {gamepad_type_t::type_e::PS4, "Sony PS4 Controller (Wireless dongle)"}},
    {{0x0f0d, 0x008a}, {gamepad_type_t::type_e::PS4, "HORI Real Arcade Pro 4"}},
    {{0x0f0d, 0x0055}, {gamepad_type_t::type_e::PS4, "HORIPAD 4 FPS"}},
    {{0x0f0d, 0x0066}, {gamepad_type_t::type_e::PS4, "HORIPAD 4 FPS Plus"}},
    {{0x0738, 0x8384}, {gamepad_type_t::type_e::PS4, "HORIPAD 4 FPS Plus"}},
    {{0x0738, 0x8250}, {gamepad_type_t::type_e::PS4, "Mad Catz FightPad Pro PS4"}},
    {{0x0738, 0x8384}, {gamepad_type_t::type_e::PS4, "Mad Catz Fightstick TE S+"}},
    {{0x0C12, 0x0E10}, {gamepad_type_t::type_e::PS4, "Armor Armor 3 Pad PS4"}},
    {{0x0C12, 0x1CF6}, {gamepad_type_t::type_e::PS4, "EMIO PS4 Elite Controller"}},
    {{0x1532, 0x1000}, {gamepad_type_t::type_e::PS4, "Razer Raiju PS4 Controller"}},
    {{0x1532, 0X0401}, {gamepad_type_t::type_e::PS4, "Razer Panthera PS4 Controller"}},
    {{0x054c, 0x05c5}, {gamepad_type_t::type_e::PS4, "STRIKEPAD PS4 Grip Add-on"}},
    {{0x146b, 0x0d01}, {gamepad_type_t::type_e::PS4, "Nacon Revolution Pro Controller"}},
    {{0x146b, 0x0d02}, {gamepad_type_t::type_e::PS4, "Nacon Revolution Pro Controller V2"}},
    {{0x0f0d, 0x00a0}, {gamepad_type_t::type_e::PS4, "HORI TAC4"}},
    {{0x0f0d, 0x009c}, {gamepad_type_t::type_e::PS4, "HORI TAC PRO"}},
    {{0x0c12, 0x0ef6}, {gamepad_type_t::type_e::PS4, "Hitbox Arcade Stick"}},
    {{0x0079, 0x181b}, {gamepad_type_t::type_e::PS4, "Venom Arcade Stick"}},
    {{0x0738, 0x3250}, {gamepad_type_t::type_e::PS4, "Mad Catz FightPad PRO"}},
    {{0x0f0d, 0x00ee}, {gamepad_type_t::type_e::PS4, "HORI mini wired gamepad"}},
    {{0x0738, 0x8481}, {gamepad_type_t::type_e::PS4, "Mad Catz FightStick TE 2+ PS4"}},
    {{0x0738, 0x8480}, {gamepad_type_t::type_e::PS4, "Mad Catz FightStick TE 2"}},
    {{0x7545, 0x0104}, {gamepad_type_t::type_e::PS4, "Armor 3, Level Up Cobra"}},
    {{0x1532, 0x1007}, {gamepad_type_t::type_e::PS4, "Razer Raiju 2 Tournament Edition (USB)"}},
    {{0x1532, 0x100A}, {gamepad_type_t::type_e::PS4, "Razer Raiju 2 Tournament Edition (BT)"}},
    {{0x1532, 0x1004}, {gamepad_type_t::type_e::PS4, "Razer Raiju 2 Ultimate Edition (USB)"}},
    {{0x1532, 0x1009}, {gamepad_type_t::type_e::PS4, "Razer Raiju 2 Ultimate Edition (BT)"}},
    {{0x1532, 0x1008}, {gamepad_type_t::type_e::PS4, "Razer Panthera Evo Fightstick"}},
    {{0x9886, 0x0025}, {gamepad_type_t::type_e::PS4, "Astro C40"}},
    {{0x0c12, 0x0e15}, {gamepad_type_t::type_e::PS4, "Game:Pad 4"}},
    {{0x4001, 0x0104}, {gamepad_type_t::type_e::PS4, "PS4 Fun Controller"}},
    {{0x056e, 0x2004}, {gamepad_type_t::type_e::Xbox360, "Elecom JC-U3613M"}},
    {{0x06a3, 0xf51a}, {gamepad_type_t::type_e::Xbox360, "Saitek P3600"}},
    {{0x0738, 0x4716}, {gamepad_type_t::type_e::Xbox360, "Mad Catz Wired Xbox 360 Controller"}},
    {{0x0738, 0x4718}, {gamepad_type_t::type_e::Xbox360, "Mad Catz Street Fighter IV FightStick SE"}},
    {{0x0738, 0x4726}, {gamepad_type_t::type_e::Xbox360, "Mad Catz Xbox 360 Controller"}},
    {{0x0738, 0x4728}, {gamepad_type_t::type_e::Xbox360, "Mad Catz Street Fighter IV FightPad"}},
    {{0x0738, 0x4736}, {gamepad_type_t::type_e::Xbox360, "Mad Catz MicroCon Gamepad"}},
    {{0x0738, 0x4738}, {gamepad_type_t::type_e::Xbox360, "Mad Catz Wired Xbox 360 Controller (SFIV)"}},
    {{0x0738, 0x4740}, {gamepad_type_t::type_e::Xbox360, "Mad Catz Beat Pad"}},
    {{0x0738, 0x4a01}, {gamepad_type_t::type_e::XboxOne, "Mad Catz FightStick TE 2"}},
    {{0x0738, 0xb726}, {gamepad_type_t::type_e::Xbox360, "Mad Catz Xbox controller - MW2"}},
    {{0x0738, 0xbeef}, {gamepad_type_t::type_e::Xbox360, "Mad Catz JOYTECH NEO SE Advanced GamePad"}},
    {{0x0738, 0xcb02}, {gamepad_type_t::type_e::Xbox360, "Saitek Cyborg Rumble Pad - PC/Xbox 360"}},
    {{0x0738, 0xcb03}, {gamepad_type_t::type_e::Xbox360, "Saitek P3200 Rumble Pad - PC/Xbox 360"}},
    {{0x0738, 0xf738}, {gamepad_type_t::type_e::Xbox360, "Super SFIV FightStick TE S"}},
    {{0x0e6f, 0x0105}, {gamepad_type_t::type_e::Xbox360, "HSM3 Xbox360 dancepad"}},
    {{0x0e6f, 0x0113}, {gamepad_type_t::type_e::Xbox360, "Afterglow AX.1 Gamepad for Xbox 360"}},
    {{0x0e6f, 0x011f}, {gamepad_type_t::type_e::Xbox360, "Rock Candy Gamepad Wired Controller"}},
    {{0x0e6f, 0x0133}, {gamepad_type_t::type_e::Xbox360, "Xbox 360 Wired Controller"}},
    {{0x0e6f, 0x0139}, {gamepad_type_t::type_e::XboxOne, "Afterglow Prismatic Wired Controller"}},
    {{0x0e6f, 0x013a}, {gamepad_type_t::type_e::XboxOne, "PDP Xbox One Controller"}},
    {{0x0e6f, 0x0146}, {gamepad_type_t::type_e::XboxOne, "Rock Candy Wired Controller for Xbox One"}},
    {{0x0e6f, 0x0147}, {gamepad_type_t::type_e::XboxOne, "PDP Marvel Xbox One Controller"}},
    {{0x0e6f, 0x015c}, {gamepad_type_t::type_e::XboxOne, "PDP Xbox One Arcade Stick"}},
    {{0x0e6f, 0x0161}, {gamepad_type_t::type_e::XboxOne, "PDP Xbox One Controller"}},
    {{0x0e6f, 0x0162}, {gamepad_type_t::type_e::XboxOne, "PDP Xbox One Controller"}},
    {{0x0e6f, 0x0163}, {gamepad_type_t::type_e::XboxOne, "PDP Xbox One Controller"}},
    {{0x0e6f, 0x0164}, {gamepad_type_t::type_e::XboxOne, "PDP Battlefield One"}},
    {{0x0e6f, 0x0165}, {gamepad_type_t::type_e::XboxOne, "PDP Titanfall 2"}},
    {{0x0e6f, 0x0201}, {gamepad_type_t::type_e::Xbox360, "Pelican PL-3601 'TSZ' Wired Xbox 360 Controller"}},
    {{0x0e6f, 0x0213}, {gamepad_type_t::type_e::Xbox360, "Afterglow Gamepad for Xbox 360"}},
    {{0x0e6f, 0x021f}, {gamepad_type_t::type_e::Xbox360, "Rock Candy Gamepad for Xbox 360"}},
    {{0x0e6f, 0x0246}, {gamepad_type_t::type_e::XboxOne, "Rock Candy Gamepad for Xbox One 2015"}},
    {{0x0e6f, 0x0301}, {gamepad_type_t::type_e::Xbox360, "Logic3 Controller"}},
    {{0x0e6f, 0x0346}, {gamepad_type_t::type_e::XboxOne, "Rock Candy Gamepad for Xbox One 2016"}},
    {{0x0e6f, 0x0401}, {gamepad_type_t::type_e::Xbox360, "Logic3 Controller"}},
    {{0x0e6f, 0x0413}, {gamepad_type_t::type_e::Xbox360, "Afterglow AX.1 Gamepad for Xbox 360"}},
    {{0x0e6f, 0x0501}, {gamepad_type_t::type_e::Xbox360, "PDP Xbox 360 Controller"}},
    {{0x0e6f, 0xf900}, {gamepad_type_t::type_e::Xbox360, "PDP Afterglow AX.1"}},
    {{0x0f0d, 0x000a}, {gamepad_type_t::type_e::Xbox360, "Hori Co. DOA4 FightStick"}},
    {{0x0f0d, 0x000c}, {gamepad_type_t::type_e::Xbox360, "Hori PadEX Turbo"}},
    {{0x0f0d, 0x000d}, {gamepad_type_t::type_e::Xbox360, "Hori Fighting Stick EX2"}},
    {{0x0f0d, 0x0016}, {gamepad_type_t::type_e::Xbox360, "Hori Real Arcade Pro.EX"}},
    {{0x0f0d, 0x001b}, {gamepad_type_t::type_e::Xbox360, "Hori Real Arcade Pro VX"}},
    {{0x0f0d, 0x0063}, {gamepad_type_t::type_e::XboxOne, "Hori Real Arcade Pro Hayabusa (USA) Xbox One"}},
    {{0x0f0d, 0x0067}, {gamepad_type_t::type_e::XboxOne, "HORIPAD ONE"}},
    {{0x0f0d, 0x0078}, {gamepad_type_t::type_e::XboxOne, "Hori Real Arcade Pro V Kai Xbox One"}},
    {{0x11c9, 0x55f0}, {gamepad_type_t::type_e::Xbox360, "Nacon GC-100XF"}},
    {{0x12ab, 0x0004}, {gamepad_type_t::type_e::Xbox360, "Honey Bee Xbox360 dancepad"}},
    {{0x12ab, 0x0301}, {gamepad_type_t::type_e::Xbox360, "PDP AFTERGLOW AX.1"}},
    {{0x12ab, 0x0303}, {gamepad_type_t::type_e::Xbox360, "Mortal Kombat Klassic FightStick"}},
    {{0x1430, 0x4748}, {gamepad_type_t::type_e::Xbox360, "RedOctane Guitar Hero X-plorer"}},
    {{0x1430, 0xf801}, {gamepad_type_t::type_e::Xbox360, "RedOctane Controller"}},
    {{0x146b, 0x0601}, {gamepad_type_t::type_e::Xbox360, "BigBen Interactive XBOX 360 Controller"}},
    {{0x1532, 0x0037}, {gamepad_type_t::type_e::Xbox360, "Razer Sabertooth"}},
    {{0x1532, 0x0a00}, {gamepad_type_t::type_e::XboxOne, "Razer Atrox Arcade Stick"}},
    {{0x1532, 0x0a03}, {gamepad_type_t::type_e::XboxOne, "Razer Wildcat"}},
    {{0x15e4, 0x3f00}, {gamepad_type_t::type_e::Xbox360, "Power A Mini Pro Elite"}},
    {{0x15e4, 0x3f0a}, {gamepad_type_t::type_e::Xbox360, "Xbox Airflo wired controller"}},
    {{0x15e4, 0x3f10}, {gamepad_type_t::type_e::Xbox360, "Batarang Xbox 360 controller"}},
    {{0x162e, 0xbeef}, {gamepad_type_t::type_e::Xbox360, "Joytech Neo-Se Take2"}},
    {{0x1689, 0xfd00}, {gamepad_type_t::type_e::Xbox360, "Razer Onza Tournament Edition"}},
    {{0x1689, 0xfd01}, {gamepad_type_t::type_e::Xbox360, "Razer Onza Classic Edition"}},
    {{0x1689, 0xfe00}, {gamepad_type_t::type_e::Xbox360, "Razer Sabertooth"}},
    {{0x1bad, 0x0002}, {gamepad_type_t::type_e::Xbox360, "Harmonix Rock Band Guitar"}},
    {{0x1bad, 0x0003}, {gamepad_type_t::type_e::Xbox360, "Harmonix Rock Band Drumkit"}},
    {{0x1bad, 0xf016}, {gamepad_type_t::type_e::Xbox360, "Mad Catz Xbox 360 Controller"}},
    {{0x1bad, 0xf018}, {gamepad_type_t::type_e::Xbox360, "Mad Catz Street Fighter IV SE Fighting Stick"}},
    {{0x1bad, 0xf019}, {gamepad_type_t::type_e::Xbox360, "Mad Catz Brawlstick for Xbox 360"}},
    {{0x1bad, 0xf021}, {gamepad_type_t::type_e::Xbox360, "Mad Cats Ghost Recon FS GamePad"}},
    {{0x1bad, 0xf023}, {gamepad_type_t::type_e::Xbox360, "MLG Pro Circuit Controller (Xbox)"}},
    {{0x1bad, 0xf025}, {gamepad_type_t::type_e::Xbox360, "Mad Catz Call Of Duty"}},
    {{0x1bad, 0xf027}, {gamepad_type_t::type_e::Xbox360, "Mad Catz FPS Pro"}},
    {{0x1bad, 0xf028}, {gamepad_type_t::type_e::Xbox360, "Street Fighter IV FightPad"}},
    {{0x1bad, 0xf02e}, {gamepad_type_t::type_e::Xbox360, "Mad Catz Fightpad"}},
    {{0x1bad, 0xf036}, {gamepad_type_t::type_e::Xbox360, "Mad Catz MicroCon GamePad Pro"}},
    {{0x1bad, 0xf038}, {gamepad_type_t::type_e::Xbox360, "Street Fighter IV FightStick TE"}},
    {{0x1bad, 0xf039}, {gamepad_type_t::type_e::Xbox360, "Mad Catz MvC2 TE"}},
    {{0x1bad, 0xf03a}, {gamepad_type_t::type_e::Xbox360, "Mad Catz SFxT Fightstick Pro"}},
    {{0x1bad, 0xf03d}, {gamepad_type_t::type_e::Xbox360, "Street Fighter IV Arcade Stick TE - Chun Li"}},
    {{0x1bad, 0xf03e}, {gamepad_type_t::type_e::Xbox360, "Mad Catz MLG FightStick TE"}},
    {{0x1bad, 0xf03f}, {gamepad_type_t::type_e::Xbox360, "Mad Catz FightStick SoulCaliber"}},
    {{0x1bad, 0xf042}, {gamepad_type_t::type_e::Xbox360, "Mad Catz FightStick TES+"}},
    {{0x1bad, 0xf080}, {gamepad_type_t::type_e::Xbox360, "Mad Catz FightStick TE2"}},
    {{0x1bad, 0xf501}, {gamepad_type_t::type_e::Xbox360, "HoriPad EX2 Turbo"}},
    {{0x1bad, 0xf502}, {gamepad_type_t::type_e::Xbox360, "Hori Real Arcade Pro.VX SA"}},
    {{0x1bad, 0xf503}, {gamepad_type_t::type_e::Xbox360, "Hori Fighting Stick VX"}},
    {{0x1bad, 0xf504}, {gamepad_type_t::type_e::Xbox360, "Hori Real Arcade Pro. EX"}},
    {{0x1bad, 0xf505}, {gamepad_type_t::type_e::Xbox360, "Hori Fighting Stick EX2B"}},
    {{0x1bad, 0xf506}, {gamepad_type_t::type_e::Xbox360, "Hori Real Arcade Pro.EX Premium VLX"}},
    {{0x1bad, 0xf900}, {gamepad_type_t::type_e::Xbox360, "Harmonix Xbox 360 Controller"}},
    {{0x1bad, 0xf901}, {gamepad_type_t::type_e::Xbox360, "Gamestop Xbox 360 Controller"}},
    {{0x1bad, 0xf903}, {gamepad_type_t::type_e::Xbox360, "Tron Xbox 360 controller"}},
    {{0x1bad, 0xf904}, {gamepad_type_t::type_e::Xbox360, "PDP Versus Fighting Pad"}},
    {{0x1bad, 0xf906}, {gamepad_type_t::type_e::Xbox360, "MortalKombat FightStick"}},
    {{0x1bad, 0xfa01}, {gamepad_type_t::type_e::Xbox360, "MadCatz GamePad"}},
    {{0x1bad, 0xfd00}, {gamepad_type_t::type_e::Xbox360, "Razer Onza TE"}},
    {{0x1bad, 0xfd01}, {gamepad_type_t::type_e::Xbox360, "Razer Onza"}},
    {{0x24c6, 0x5000}, {gamepad_type_t::type_e::Xbox360, "Razer Atrox Arcade Stick"}},
    {{0x24c6, 0x5300}, {gamepad_type_t::type_e::Xbox360, "PowerA MINI PROEX Controller"}},
    {{0x24c6, 0x5303}, {gamepad_type_t::type_e::Xbox360, "Xbox Airflo wired controller"}},
    {{0x24c6, 0x530a}, {gamepad_type_t::type_e::Xbox360, "Xbox 360 Pro EX Controller"}},
    {{0x24c6, 0x531a}, {gamepad_type_t::type_e::Xbox360, "PowerA Pro Ex"}},
    {{0x24c6, 0x5397}, {gamepad_type_t::type_e::Xbox360, "FUS1ON Tournament Controller"}},
    {{0x24c6, 0x541a}, {gamepad_type_t::type_e::XboxOne, "PowerA Xbox One Mini Wired Controller"}},
    {{0x24c6, 0x542a}, {gamepad_type_t::type_e::XboxOne, "Xbox ONE spectra"}},
    {{0x24c6, 0x543a}, {gamepad_type_t::type_e::XboxOne, "PowerA Xbox One wired controller"}},
    {{0x24c6, 0x5500}, {gamepad_type_t::type_e::Xbox360, "Hori XBOX 360 EX 2 with Turbo"}},
    {{0x24c6, 0x5501}, {gamepad_type_t::type_e::Xbox360, "Hori Real Arcade Pro VX-SA"}},
    {{0x24c6, 0x5502}, {gamepad_type_t::type_e::Xbox360, "Hori Fighting Stick VX Alt"}},
    {{0x24c6, 0x5503}, {gamepad_type_t::type_e::Xbox360, "Hori Fighting Edge"}},
    {{0x24c6, 0x5506}, {gamepad_type_t::type_e::Xbox360, "Hori SOULCALIBUR V Stick"}},
    {{0x24c6, 0x550d}, {gamepad_type_t::type_e::Xbox360, "Hori GEM Xbox controller"}},
    {{0x24c6, 0x550e}, {gamepad_type_t::type_e::Xbox360, "Hori Real Arcade Pro V Kai 360"}},
    {{0x24c6, 0x551a}, {gamepad_type_t::type_e::XboxOne, "PowerA FUSION Pro Controller"}},
    {{0x24c6, 0x561a}, {gamepad_type_t::type_e::XboxOne, "PowerA FUSION Controller"}},
    {{0x24c6, 0x5b02}, {gamepad_type_t::type_e::Xbox360, "Thrustmaster"}},
    {{0x24c6, 0x5b03}, {gamepad_type_t::type_e::Xbox360, "Thrustmaster Ferrari 458 Racing Wheel"}},
    {{0x24c6, 0x5d04}, {gamepad_type_t::type_e::Xbox360, "Razer Sabertooth"}},
    {{0x24c6, 0xfafe}, {gamepad_type_t::type_e::Xbox360, "Rock Candy Gamepad for Xbox 360"}}
};

#if defined(WIN64) || defined(_WIN64) || defined(__MINGW64__) \
 || defined(WIN32) || defined(_WIN32) || defined(__MINGW32__)

//#define GAMEPAD_ENABLE_DIRECTINPUT
//#define GAMEPAD_ENABLE_HID
//#define GAMEPAD_ENABLE_XINPUT

#ifdef GAMEPAD_ENABLE_XINPUT

#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <Windows.h>

// Xinput9_1_0.lib for xinput1_3.dll (>= Win7) xinput.lib for xinput1_4.dll (>= Win10)
//#pragma comment(lib, "Xinput9_1_0.lib")

#include <Xinput.h>

#ifndef XINPUT_GAMEPAD_GUIDE
#define XINPUT_GAMEPAD_GUIDE 0x0400
#endif

#define XINPUT1_3_XInputGetStateEx_ORDINAL ((LPCSTR)100)
#define XINPUT1_4_XInputGetStateEx_ORDINAL ((LPCSTR)100)

class XInput_Lib
{
    HMODULE _hDll;
    
    XInput_Lib()
    {
        _hDll = LoadLibraryA("xinput1_3.dll");
        XInputGetStateEx = (decltype(XInputGetStateEx))GetProcAddress(_hDll, XINPUT1_3_XInputGetStateEx_ORDINAL);
        XInputSetState = (decltype(XInputSetState))GetProcAddress(_hDll, "XInputSetState");

        //_hDll = LoadLibraryA("xinput1_4.dll");
        //XInputGetStateEx = (decltype(XInputGetStateEx))GetProcAddress(_hDll, XINPUT1_4_XInputGetStateEx_ORDINAL);
        //XInputSetState = (decltype(XInputSetState))GetProcAddress(_hDll, "XInputSetState");
    }
public:
    ~XInput_Lib()
    {
        FreeLibrary(_hDll);
    }

    static XInput_Lib& Inst()
    {
        static XInput_Lib inst;
        return inst;
    }

    decltype(XInputGetState)* XInputGetStateEx;
    decltype(XInputSetState)* XInputSetState;
};

class XInput_Gamepad : public Gamepad
{
    DWORD _xinput_id;

public:
    XInput_Gamepad(DWORD xinput_id) :
        _xinput_id(xinput_id)
    {}

    virtual ~XInput_Gamepad()
    {}

    virtual int GetXinputId()
    {
        return _xinput_id;
    }

    virtual bool RunFrame()
    {
        DWORD dwResult;

        XINPUT_STATE state = { 0 };
        ZeroMemory(&state, sizeof(XINPUT_STATE));

        // Simply get the state of the controller from XInput.
        dwResult = XInput_Lib::Inst().XInputGetStateEx(_xinput_id, &state);

        if (dwResult != ERROR_SUCCESS)
        {
            left_stick.x = 0.0f;
            left_stick.y = 0.0f;

            right_stick.x = 0.0f;
            right_stick.y = 0.0f;

            up = false;
            down = false;
            left = false;
            right = false;

            start = false;
            back = false;

            left_thumb = false;
            right_thumb = false;

            left_shoulder = false;
            right_shoulder = false;

            a = false;
            b = false;
            x = false;
            y = false;

            guide = false;

            left_trigger = 0.0f;
            right_trigger = 0.0f;

            return false;
        }

        left_stick.x = state.Gamepad.sThumbLX / (state.Gamepad.sThumbLX > 0 ? 32767.0f : 32768.0f);
        left_stick.y = state.Gamepad.sThumbLY / (state.Gamepad.sThumbLY > 0 ? 32767.0f : 32768.0f);

        right_stick.x = state.Gamepad.sThumbRX / (state.Gamepad.sThumbRX > 0 ? 32767.0f : 32768.0f);
        right_stick.y = state.Gamepad.sThumbRY / (state.Gamepad.sThumbRY > 0 ? 32767.0f : 32768.0f);

        up = state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP;
        down = state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN;
        left = state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT;
        right = state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT;

        start = state.Gamepad.wButtons & XINPUT_GAMEPAD_START;
        back = state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK;

        left_thumb = state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB;
        right_thumb = state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB;

        left_shoulder = state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER;
        right_shoulder = state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER;

        guide = state.Gamepad.wButtons & XINPUT_GAMEPAD_GUIDE;

        a = state.Gamepad.wButtons & XINPUT_GAMEPAD_A;
        b = state.Gamepad.wButtons & XINPUT_GAMEPAD_B;
        x = state.Gamepad.wButtons & XINPUT_GAMEPAD_X;
        y = state.Gamepad.wButtons & XINPUT_GAMEPAD_Y;

        left_trigger = state.Gamepad.bLeftTrigger / 255.0f;
        right_trigger = state.Gamepad.bRightTrigger / 255.0f;

        return true;
    }

    virtual bool SetVibration(uint16_t left_speed, uint16_t right_speed)
    {
        XINPUT_VIBRATION vibration;
        ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));
        vibration.wLeftMotorSpeed = left_speed;
        vibration.wRightMotorSpeed = right_speed;
        return XInput_Lib::Inst().XInputSetState(_xinput_id, &vibration) == ERROR_SUCCESS;
    }

    virtual bool SetLed(uint8_t r, uint8_t g, uint8_t b)
    {
        return false;
    }
};

#endif//GAMEPAD_ENABLE_XINPUT

#ifdef GAMEPAD_ENABLE_DIRECTINPUT

// Will need dinput8.lib and dxguid.lib
//#pragma comment(lib, "dinput8.lib")
//#pragma comment(lib, "dxguid.lib")

#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>

#include <wbemidl.h>
#include <OleAuto.h>

//#pragma comment(lib, "Ole32.lib")
//#pragma comment(lib, "OleAut32.lib")

class DirectInput_Manager
{
    struct device_info_t
    {
        GUID device_guid;
        uint16_t vid;
        uint16_t pid;
    };
    std::vector<device_info_t> controllers;
    std::vector<std::wstring> xinput_paths;

    IDirectInput8* _dev;


    DirectInput_Manager()
    {
        DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&_dev, NULL);
    }

    BOOL AddXInputPath(LONG vidpid)
    {
        IWbemLocator* pIWbemLocator = NULL;
        IEnumWbemClassObject* pEnumDevices = NULL;
        IWbemClassObject* pDevices[20] = { 0 };
        IWbemServices* pIWbemServices = NULL;
        BSTR                    bstrNamespace = NULL;
        BSTR                    bstrDeviceID = NULL;
        BSTR                    bstrClassName = NULL;
        DWORD                   uReturned = 0;
        bool                    bIsXinputDevice = false;
        UINT                    iDevice = 0;
        VARIANT                 var;
        HRESULT                 hr;

        // CoInit if needed
        hr = CoInitialize(NULL);
        bool bCleanupCOM = SUCCEEDED(hr);

        // Create WMI
        hr = CoCreateInstance(__uuidof(WbemLocator), NULL, CLSCTX_INPROC_SERVER, __uuidof(IWbemLocator), (LPVOID*)&pIWbemLocator);
        if (FAILED(hr) || pIWbemLocator == NULL)
            goto LCleanup;

        bstrNamespace = SysAllocString(L"\\\\.\\root\\cimv2"); if (bstrNamespace == NULL) goto LCleanup;
        bstrClassName = SysAllocString(L"Win32_PNPEntity");   if (bstrClassName == NULL) goto LCleanup;
        bstrDeviceID = SysAllocString(L"DeviceID");          if (bstrDeviceID == NULL)  goto LCleanup;

        // Connect to WMI 
        hr = pIWbemLocator->ConnectServer(bstrNamespace, NULL, NULL, 0L,
            0L, NULL, NULL, &pIWbemServices);
        if (FAILED(hr) || pIWbemServices == NULL)
            goto LCleanup;

        // Switch security level to IMPERSONATE. 
        CoSetProxyBlanket(pIWbemServices, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL,
            RPC_C_AUTHN_LEVEL_CALL, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE);

        hr = pIWbemServices->CreateInstanceEnum(bstrClassName, 0, NULL, &pEnumDevices);
        if (FAILED(hr) || pEnumDevices == NULL)
            goto LCleanup;

        // Loop over all devices
        for (;; )
        {
            // Get 20 at a time
            hr = pEnumDevices->Next(10000, 20, pDevices, &uReturned);
            if (FAILED(hr))
                goto LCleanup;
            if (uReturned == 0)
                break;

            for (iDevice = 0; iDevice < uReturned; iDevice++)
            {
                // For each device, get its device ID
                hr = pDevices[iDevice]->Get(bstrDeviceID, 0L, &var, NULL, NULL);
                if (SUCCEEDED(hr) && var.vt == VT_BSTR && var.bstrVal != NULL)
                {
                    // Check if the device ID contains "IG_".  If it does, then it's an XInput device
                        // This information can not be found from DirectInput 
                    if (wcsstr(var.bstrVal, L"IG_"))
                    {
                        // If it does, then get the VID/PID from var.bstrVal
                        DWORD dwPid = 0, dwVid = 0;
                        WCHAR* strVid = wcsstr(var.bstrVal, L"VID_");
                        if (strVid && swscanf(strVid, L"VID_%4X", &dwVid) != 1)
                            dwVid = 0;
                        WCHAR* strPid = wcsstr(var.bstrVal, L"PID_");
                        if (strPid && swscanf(strPid, L"PID_%4X", &dwPid) != 1)
                            dwPid = 0;

                        // Compare the VID/PID to the DInput device
                        DWORD dwVidPid = MAKELONG(dwVid, dwPid);
                        if (dwVidPid == vidpid)
                        {
                            xinput_paths.push_back(var.bstrVal);

                            bIsXinputDevice = true;
                            goto LCleanup;
                        }
                    }
                }
                if (pDevices[iDevice]) pDevices[iDevice]->Release();
            }
        }

    LCleanup:
        if (bstrNamespace)
            SysFreeString(bstrNamespace);
        if (bstrDeviceID)
            SysFreeString(bstrDeviceID);
        if (bstrClassName)
            SysFreeString(bstrClassName);
        for (iDevice = 0; iDevice < 20; iDevice++)
            if (pDevices[iDevice]) pDevices[iDevice]->Release();
        if (pEnumDevices) pEnumDevices->Release();
        if (pIWbemLocator) pIWbemLocator->Release();
        if (pIWbemServices) pIWbemServices->Release();

        if (bCleanupCOM)
            CoUninitialize();

        return bIsXinputDevice;
    }

    static BOOL CALLBACK enumerate_device_callback(LPCDIDEVICEINSTANCE instance, LPVOID user_param)
    {
        DirectInput_Manager* _this = (DirectInput_Manager*)user_param;

        uint16_t vid = instance->guidProduct.Data1 & 0xFFFF;
        uint16_t pid = (instance->guidProduct.Data1 >> 16) & 0xFFFF;
#ifdef GAMEPAD_ENABLE_XINPUT
        if (!_this->AddXInputPath(MAKELONG(vid, pid)))
            _this->controllers.push_back({ instance->guidInstance, vid, pid });
#else
        _this->controllers.push_back({ instance->guidInstance, vid, pid });
#endif

        return DIENUM_CONTINUE;
    }

public:
    static DirectInput_Manager& Inst()
    {
        static DirectInput_Manager inst;
        return inst;
    }

    ~DirectInput_Manager()
    {
        if(_dev != nullptr)
            _dev->Release();
    }

    IDirectInputDevice8* create_device(GUID guidIstance)
    {
        IDirectInputDevice8* device;
        _dev->CreateDevice(guidIstance, &device, NULL);

        return device;
    }

    std::vector<device_info_t>const& enumerate_device(std::vector<std::wstring> &xinput_paths)
    {
        controllers.clear();

        //(LPDIENUMDEVICESCALLBACK)
        _dev->EnumDevices(DI8DEVCLASS_GAMECTRL, &enumerate_device_callback, this, DIEDFL_ATTACHEDONLY);

        xinput_paths = this->xinput_paths;

        for (auto& path : xinput_paths)
        {
            for(auto &c: path)
                c = towlower(c);
        }

        return controllers;
    }
};

class DirectInput_Gamepad : public Gamepad
{
    IDirectInputDevice8* _dev;
    DWORD _numFFAxis;

    static BOOL CALLBACK EnumFFAxesCallback(LPCDIDEVICEOBJECTINSTANCE lpddoi, LPVOID pvRef)
    {
        DWORD& numFFAxis = *(DWORD*)pvRef;

        if (((lpddoi->dwFlags & DIDOI_FFACTUATOR) != 0))
            ++numFFAxis;

        return DIENUM_CONTINUE;
    }

    static BOOL CALLBACK EnumObjectCallback(LPCDIDEVICEOBJECTINSTANCE lpddoi, LPVOID pvRef)
    {
        DirectInput_Gamepad *_this = (DirectInput_Gamepad*)pvRef;

        if ((lpddoi->dwFlags & DIDFT_AXIS) != 0)
        {
            if(lpddoi->guidType == GUID_XAxis)
            {
                
            }
            else if (lpddoi->guidType == GUID_YAxis)
            {

            }
            else if (lpddoi->guidType == GUID_RxAxis)
            {

            }
            else if (lpddoi->guidType == GUID_RyAxis)
            {

            }
            else if (lpddoi->guidType == GUID_ZAxis)
            {

            }
            else if (lpddoi->guidType == GUID_RzAxis)
            {

            }
        }

        return DIENUM_CONTINUE;
    }

public:
    DirectInput_Gamepad(GUID guidInstance, uint16_t vid, uint16_t pid):
        _dev(nullptr),
        _numFFAxis(0)
    {
        id.productID = pid;
        id.vendorID = pid;

        _dev = DirectInput_Manager::Inst().create_device(guidInstance);
        _dev->SetDataFormat(&c_dfDIJoystick);

        DIDEVCAPS deviceCaps;
        deviceCaps.dwSize = sizeof(DIDEVCAPS);

        HRESULT hr = _dev->GetCapabilities(&deviceCaps);
        bool ffSupported = ((deviceCaps.dwFlags & DIDC_FORCEFEEDBACK) == DIDC_FORCEFEEDBACK);
        if (!ffSupported)
        {
            hr = _dev->EnumObjects(EnumFFAxesCallback, (VOID*)&_numFFAxis, DIDFT_AXIS);
            
            if (_numFFAxis > 2)
                _numFFAxis = 2;

            if (!_numFFAxis)
            {
                //PrintLog("ForceFeedback unsupported");
                //m_pController->m_useforce = false;
                //return false;
            }
            else
            {
                //m_pController->m_pDevice->EnumEffects(EnumEffectsCallback, this, DIEFT_ALL);
                //m_pController->m_pDevice->SendForceFeedbackCommand(DISFFC_RESET);
                //m_pController->m_pDevice->SendForceFeedbackCommand(DISFFC_SETACTUATORSON);
                //return true;
            }
        }

        hr = _dev->EnumObjects(EnumObjectCallback, (VOID*)this, DIDFT_ALL);
    }

    virtual ~DirectInput_Gamepad()
    {
        if(_dev != nullptr)
            _dev->Release();
    }

    virtual int GetXinputId()
    {
        return -1;
    }

    virtual bool RunFrame()
    {
        DIJOYSTATE state;

        if (_dev->Acquire() == S_OK)
        {
            _dev->GetDeviceState(sizeof(state), &state);
            _dev->Unacquire();

            int16_t value;
            value = (state.lX - 32767);
            left_stick.x = value / (value > 0 ? 32767.0f : 32768.0f);
            value = (~state.lY - 32767);
            left_stick.y = value / (value > 0 ? 32767.0f : 32768.0f);

            value = (state.lRx - 32767);
            right_stick.x = value / (value > 0 ? 32767.0f : 32768.0f);
            value = (~state.lRy - 32767);
            right_stick.y = value / (value > 0 ? 32767.0f : 32768.0f);

            switch (state.rgdwPOV[0])
            {
                case 0xFFFFFFFF: up = down = left = right = false; break;
                case 0    : up = true; down = left = right = false; break;
                case 4500 : up = right = true; left = down = false; break;
                case 9000 : right = true; up = down = left = false; break;
                case 13500: down = right = true; left = up = false; break;
                case 18000: down = true; right = left = up = false; break;
                case 22500: down = left = true; right = up = false; break;
                case 27000: left = true; up = down = right = false; break;
                case 31500: left = up = true; down = right = false; break;
            }
        }

        return true;
    }

    virtual bool SetVibration(uint16_t, uint16_t)
    {
        return true;
    }

    virtual bool SetLed(uint8_t r, uint8_t g, uint8_t b)
    {
        return false;
    }
};

#endif//GAMEPAD_ENABLE_DIRECTINPUT

#ifdef GAMEPAD_ENABLE_HID

// Basically its an implementation like wine's xinput
// It should do the same as DirectInput but does not depend on DirectInput

// Will need SetupAPI.lib and hid.lib
//#pragma comment(lib, "SetupAPI.lib")
//#pragma comment(lib, "hid.lib")

#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <Windows.h>

#define INITGUID
#include <hidsdi.h>
#include <hidclass.h>
#include <SetupAPI.h>

class Hid_Device : public Gamepad
{
    struct axis_info
    {
        LONG min;
        LONG range;
        USHORT bits;
    };

    bool       _shared;
    bool       _dead;
    HANDLE     _hDevice;
    bool       _non_blocking;
    bool       _has_input;
    uint8_t* _in_buffers[2];
    int        _current_buffer;
    int        _buffer_len;
    OVERLAPPED _overlapped;

    std::wstring _device_path;
    PHIDP_PREPARSED_DATA _preparsed_data;
    HIDP_CAPS _caps;

    axis_info _lx, _ly, _ltrigger;
    axis_info _rx, _ry, _rtrigger;

    void set_axis_infos(WORD usage, LONG min, LONG max, USHORT bits)
    {
        axis_info info = { min, max - min, bits };

        switch (usage)
        {
            case HID_USAGE_GENERIC_X: _lx = info; break;
            case HID_USAGE_GENERIC_Y: _ly = info; break;
            case HID_USAGE_GENERIC_Z: _ltrigger = info; break;
            case HID_USAGE_GENERIC_RX: _rx = info; break;
            case HID_USAGE_GENERIC_RY: _ry = info; break;
            case HID_USAGE_GENERIC_RZ: _rtrigger = info; break;
        }
    }

    int32_t hid_read_timeout(int32_t timeout)
    {
        DWORD num_read;

        uint8_t* last_buffer = _in_buffers[(_current_buffer) % 2];
        uint8_t* target_buffer = _in_buffers[(_current_buffer + 1) % 2];

        if (!_has_input)
        {
            _has_input = true;
            memset(target_buffer, 0, _caps.InputReportByteLength);
            ResetEvent(_overlapped.hEvent);
            if (!ReadFile(_hDevice, target_buffer, _caps.InputReportByteLength, &num_read, &_overlapped)
                && GetLastError() != ERROR_IO_PENDING)
            {
                if(GetLastError() == ERROR_ACCESS_DENIED || GetLastError() == ERROR_INVALID_HANDLE)
                    _dead = true;

                CancelIo(_hDevice);
                _has_input = false;
                return -1;
            }
        }
        if (timeout >= 0 && WaitForSingleObject(_overlapped.hEvent, timeout))
            return 0;

        _has_input = false;
        if (!GetOverlappedResult(_hDevice, &_overlapped, &num_read, TRUE))
            return -1;

        return num_read;
    }

    int32_t hid_read()
    {
        return hid_read_timeout(_non_blocking ? 0 : -1);
    }

    float scale_short_axis(int16_t value, const struct axis_info* axis)
    {
        return value / (value > 0 ? 32767.0f : 32768.0f);
    }

    float scale_byte(int8_t value, const struct axis_info* axis)
    {
        return value / (value > 0 ? 127.0f : 128.0f);
    }

    void reopen_device_if_dead()
    {
        if (_dead)// Just reopen with the same parameters
            set_device(_device_path, _shared);
    }

    void close_hid_device()
    {
        if (_hDevice != INVALID_HANDLE_VALUE)
        {
            CancelIo(_hDevice);
            CloseHandle(_hDevice);
            CloseHandle(_overlapped.hEvent);
            delete[] _in_buffers[0];
            delete[] _in_buffers[1];
            HidD_FreePreparsedData(_preparsed_data);

            _hDevice = INVALID_HANDLE_VALUE;
            _dead = false;
        }
    }

public:
    Hid_Device() :
        _shared(false),
        _dead(false),
        _hDevice(INVALID_HANDLE_VALUE),
        _non_blocking(true),
        _has_input(false),
        _in_buffers{ nullptr, nullptr },
        _current_buffer(0),
        _buffer_len(0),
        _overlapped{ 0 }
    {}

    virtual ~Hid_Device()
    {
        close_hid_device();
    }

    std::wstring const& device_path() const
    {
        return _device_path;
    }

    bool set_device(std::wstring device_path, bool shared = true)
    {
        close_hid_device();

        _device_path = std::move(device_path);

        _hDevice = CreateFileW(_device_path.c_str(), GENERIC_READ | GENERIC_WRITE, shared ? FILE_SHARE_READ | FILE_SHARE_WRITE : 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, 0);
        if (_hDevice == INVALID_HANDLE_VALUE)
            return false;

        HidD_GetPreparsedData(_hDevice, &_preparsed_data);
        HidP_GetCaps(_preparsed_data, &_caps);
        
        _shared = shared;
        _overlapped.hEvent = CreateEvent(0, 0, 0, 0);
        _buffer_len = _caps.InputReportByteLength;

        _in_buffers[0] = new uint8_t[_buffer_len + 1];
        _in_buffers[1] = new uint8_t[_buffer_len + 1];

        memset(_in_buffers[0], 0, _buffer_len);
        memset(_in_buffers[1], 0, _buffer_len);

        HIDD_ATTRIBUTES hid_attributes;
        HidD_GetAttributes(_hDevice, &hid_attributes);

        id.productID = hid_attributes.ProductID;
        id.vendorID = hid_attributes.VendorID;

        USHORT value_caps_count = _caps.NumberInputValueCaps;
        PHIDP_VALUE_CAPS value_caps = (PHIDP_VALUE_CAPS)HeapAlloc(GetProcessHeap(), 0, sizeof(*value_caps) * value_caps_count);
        HidP_GetValueCaps(HidP_Input, value_caps, &value_caps_count, _preparsed_data);

        for (int i = 0; i < value_caps_count; i++)
        {
            if (value_caps[i].UsagePage != HID_USAGE_PAGE_GENERIC)
                continue;
            if (value_caps[i].IsRange)
            {
                int u;
                for (u = value_caps[i].Range.UsageMin; u <= value_caps[i].Range.UsageMax; u++)
                    set_axis_infos(u, value_caps[i].LogicalMin, value_caps[i].LogicalMax, value_caps[i].BitSize);
            }
            else
                set_axis_infos(value_caps[i].NotRange.Usage, value_caps[i].LogicalMin, value_caps[i].LogicalMax, value_caps[i].BitSize);
        }
        HeapFree(GetProcessHeap(), 0, value_caps);

        return true;
    }

    virtual int GetXinputId()
    {
        return -1;
    }

    virtual bool RunFrame()
    {
        if (_hDevice == INVALID_HANDLE_VALUE)
            return false;

        reopen_device_if_dead();
        if (hid_read() > 0)
        {
            uint8_t* last_buffer = _in_buffers[(_current_buffer) % 2];
            uint8_t* target_buffer = _in_buffers[(_current_buffer + 1) % 2];

            if (memcmp(last_buffer, target_buffer, _buffer_len) != 0)
            {
                _current_buffer = (_current_buffer + 1) % 2;

                USHORT buttons[11];
                ULONG button_len = sizeof(buttons) / sizeof(*buttons);

                HidP_GetUsages(HidP_Input, HID_USAGE_PAGE_BUTTON, 0, buttons, &button_len, _preparsed_data, (PCHAR)target_buffer, _buffer_len);

                up = false; down = false; left = false; right = false;
                start = false; back = false;
                left_shoulder = false; right_shoulder = false;
                left_thumb = false; right_thumb = false;
                a = false; b = false;
                x = false; y = false;
                guide = false;

                for (int i = 0; i < button_len; i++)
                {
                    switch (buttons[i])
                    {
                        case 1: a = true; break;
                        case 2: b = true; break;
                        case 3: x = true; break;
                        case 4: y = true; break;
                        case 5: left_shoulder = true; break;
                        case 6: right_shoulder = true; break;
                        case 7: back = true; break;
                        case 8: start = true; break;
                        case 9: left_thumb = true; break;
                        case 10: right_thumb = true; break;
                        case 11: guide = true; break;
                    }
                }

                ULONG hat_value;
                if (HidP_GetUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_HATSWITCH, &hat_value,
                    _preparsed_data, (PCHAR)target_buffer, _buffer_len) == HIDP_STATUS_SUCCESS)
                {
                    switch (hat_value) {
                        /* 8 1 2
                         * 7 0 3
                         * 6 5 4 */
                    case 0:
                        break;
                    case 1:
                        up = true;
                        break;
                    case 2:
                        up = right = true;
                        break;
                    case 3:
                        right = true;
                        break;
                    case 4:
                        right = down = true;
                        break;
                    case 5:
                        down = true;
                        break;
                    case 6:
                        down = left = true;
                        break;
                    case 7:
                        left = true;
                        break;
                    case 8:
                        left = up = true;
                        break;
                    }
                }

                LONG value;

                uint32_t res = HidP_GetScaledUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_X, &value, _preparsed_data, (PCHAR)target_buffer, _buffer_len);
                if (res == HIDP_STATUS_SUCCESS ||
                    (res == HIDP_STATUS_BAD_LOG_PHY_VALUES &&
                        HidP_GetUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_X, (PULONG)&value, _preparsed_data, (PCHAR)target_buffer, _buffer_len) == HIDP_STATUS_SUCCESS))
                {
                    left_stick.x = scale_short_axis(value - 32768, &_lx);
                }

                res = HidP_GetScaledUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_Y, &value, _preparsed_data, (PCHAR)target_buffer, _buffer_len);
                if (res == HIDP_STATUS_SUCCESS ||
                    (res == HIDP_STATUS_BAD_LOG_PHY_VALUES &&
                        HidP_GetUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_Y, (PULONG)&value, _preparsed_data, (PCHAR)target_buffer, _buffer_len) == HIDP_STATUS_SUCCESS))
                {
                    left_stick.y = scale_short_axis(~value - 32768, &_ly);
                }

                res = HidP_GetScaledUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_RX, &value, _preparsed_data, (PCHAR)target_buffer, _buffer_len);
                if (res == HIDP_STATUS_SUCCESS ||
                    (res == HIDP_STATUS_BAD_LOG_PHY_VALUES &&
                        HidP_GetUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_RX, (PULONG)&value, _preparsed_data, (PCHAR)target_buffer, _buffer_len) == HIDP_STATUS_SUCCESS))
                {
                    right_stick.x = scale_short_axis(value - 32768, &_rx);
                }

                res = HidP_GetScaledUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_RY, &value, _preparsed_data, (PCHAR)target_buffer, _buffer_len);
                if (res == HIDP_STATUS_SUCCESS ||
                    (res == HIDP_STATUS_BAD_LOG_PHY_VALUES &&
                        HidP_GetUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_RY, (PULONG)&value, _preparsed_data, (PCHAR)target_buffer, _buffer_len) == HIDP_STATUS_SUCCESS))
                {
                    right_stick.y = scale_short_axis(~value - 32768, &_ry);
                }

                res = HidP_GetScaledUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_RZ, &value, _preparsed_data, (PCHAR)target_buffer, _buffer_len);
                if (res == HIDP_STATUS_SUCCESS)
                {
                    right_trigger = scale_byte(value, &_rtrigger);
                }
                else if (res == HIDP_STATUS_BAD_LOG_PHY_VALUES &&
                    HidP_GetUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_RZ, (PULONG)&value, _preparsed_data, (PCHAR)target_buffer, _buffer_len) == HIDP_STATUS_SUCCESS)
                {
                    right_trigger = scale_short_axis(value, &_rtrigger);
                }
                    

                res = HidP_GetScaledUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_Z, &value, _preparsed_data, (PCHAR)target_buffer, _buffer_len);
                if (res == HIDP_STATUS_SUCCESS)
                {
                    left_trigger = scale_byte(value, &_ltrigger);
                }
                else if (res == HIDP_STATUS_BAD_LOG_PHY_VALUES &&
                    HidP_GetUsageValue(HidP_Input, HID_USAGE_PAGE_GENERIC, 0, HID_USAGE_GENERIC_Z, (PULONG)&value, _preparsed_data, (PCHAR)target_buffer, _buffer_len) == HIDP_STATUS_SUCCESS)
                {
                    if (value == 32768)
                        left_trigger = right_trigger = 0;
                    else if (value < 32768)
                    {
                        left_trigger = scale_short_axis(value - 32768 - 128, &_ltrigger);
                        right_trigger = 0;
                    }
                    else
                    {
                        right_trigger = scale_short_axis(value - 32768 + 127, &_ltrigger);
                        left_trigger = 0;
                    }
                }
                    
            }
        }

        return true;
    }

    virtual bool SetVibration(uint16_t left_speed, uint16_t right_speed)
    {
        if (_hDevice == INVALID_HANDLE_VALUE)
            return false;

        reopen_device_if_dead();
        if (_caps.NumberOutputValueCaps > 0)
        {
            struct {
                BYTE report;
                BYTE pad1[2];
                BYTE left;
                BYTE right;
                BYTE pad2[3];
            } report;

            // XBox360 vibration packet
            report.report = 0;
            report.pad1[0] = 0x8;
            report.pad1[1] = 0x0;
            report.left = (BYTE)(left_speed / 256);
            report.right = (BYTE)(right_speed / 256);
            memset(&report.pad2, 0, sizeof(report.pad2));

            if (HidD_SetOutputReport(_hDevice, &report, sizeof(report)))
                return true;

            return false;
        }

        return true;
    }

    virtual bool SetLed(uint8_t r, uint8_t g, uint8_t b)
    {
        return false;
    }
};

std::vector<std::wstring> find_hid_gamepads(std::vector<std::wstring>& xinput_paths)
{
    std::vector<std::wstring> gamepads;

    HDEVINFO device_info_set;
    GUID hid_guid;
    SP_DEVICE_INTERFACE_DATA interface_data;
    SP_DEVICE_INTERFACE_DETAIL_DATA_W* data;
    PHIDP_PREPARSED_DATA ppd;
    DWORD detail_size = MAX_PATH * sizeof(WCHAR);
    HANDLE device;
    HIDP_CAPS Caps;
    DWORD idx;

    hid_guid = GUID_DEVINTERFACE_HID;

    device_info_set = SetupDiGetClassDevsW(&hid_guid, NULL, NULL, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);

    data = (SP_DEVICE_INTERFACE_DETAIL_DATA_W*)HeapAlloc(GetProcessHeap(), 0, sizeof(*data) + detail_size);
    data->cbSize = sizeof(*data);

    ZeroMemory(&interface_data, sizeof(interface_data));
    interface_data.cbSize = sizeof(interface_data);

    idx = 0;
    while (SetupDiEnumDeviceInterfaces(device_info_set, NULL, &hid_guid, idx++, &interface_data))
    {
        if (!SetupDiGetDeviceInterfaceDetailW(device_info_set,
            &interface_data, data, sizeof(*data) + detail_size, NULL, NULL))
            continue;

        // Find gamepad with ig_ in their path (XBox controller handled by XInput)
        if (wcsstr(data->DevicePath, L"ig_"))
        {
            xinput_paths.push_back(data->DevicePath);
            continue;
        }

        device = CreateFileW(data->DevicePath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, 0);
        if (device == INVALID_HANDLE_VALUE)
            continue;

        HidD_GetPreparsedData(device, &ppd);
        HidP_GetCaps(ppd, &Caps);
        if (Caps.UsagePage == HID_USAGE_PAGE_GENERIC &&
            (Caps.Usage == HID_USAGE_GENERIC_GAMEPAD ||
                Caps.Usage == HID_USAGE_GENERIC_JOYSTICK ||
                Caps.Usage == HID_USAGE_GENERIC_MULTI_AXIS_CONTROLLER))
        {
            gamepads.emplace_back(data->DevicePath);
        }
        CloseHandle(device);
        HidD_FreePreparsedData(ppd);
    }
    HeapFree(GetProcessHeap(), 0, data);
    SetupDiDestroyDeviceInfoList(device_info_set);

    return gamepads;
}

#endif//GAMEPAD_ENABLE_HID

#if defined(GAMEPAD_ENABLE_DIRECTINPUT) && defined(GAMEPAD_ENABLE_HID)
    #error "Can't enable HID & DirectInput, they will conflict"
#endif

std::vector<std::shared_ptr<Gamepad>> Gamepad::get_gamepads()
{
    std::vector<std::shared_ptr<Gamepad>> gamepads;

#ifdef GAMEPAD_ENABLE_XINPUT
    DWORD dwResult;
    for (DWORD i = 0; i < XUSER_MAX_COUNT; i++)
    {
        XINPUT_STATE state;
        ZeroMemory(&state, sizeof(XINPUT_STATE));
    
        // Simply get the state of the controller from XInput.
        dwResult = XInput_Lib::Inst().XInputGetStateEx(i, &state);
    
        if (dwResult == ERROR_SUCCESS)
        {
            std::shared_ptr<Gamepad> gp(new XInput_Gamepad(i));
            gamepads.push_back(gp);
        }
    }
#endif

#ifdef GAMEPAD_ENABLE_DIRECTINPUT
    std::vector<std::wstring> xinput_paths;
    auto const& controllers_info = DirectInput_Manager::Inst().enumerate_device(xinput_paths);

    for (auto& c : controllers_info)
    {
        std::shared_ptr<DirectInput_Gamepad> gp(new DirectInput_Gamepad(c.device_guid, c.vid, c.pid));
        gamepads.emplace_back(std::move(gp));
    }
#endif

#ifdef GAMEPAD_ENABLE_HID
    std::vector<std::wstring> xinput_paths;
    std::vector<std::wstring> gamepads_infos = std::move(find_hid_gamepads(xinput_paths));

    for (auto& c : gamepads_infos)
    {
        std::shared_ptr<Hid_Device> gp(new Hid_Device());
        if(gp->set_device(c))
            gamepads.emplace_back(std::move(gp));
    }
#endif

#if defined(GAMEPAD_ENABLE_HID) || defined(GAMEPAD_ENABLE_DIRECTINPUT)
#ifdef GAMEPAD_ENABLE_XINPUT
    int xinput_index = 0;
    for (auto& gp : gamepads)
    {
        if (xinput_index < xinput_paths.size())
        {
            std::wstring const& wstr = xinput_paths[xinput_index];
            size_t pos = wstr.find(L"pid_");
            gp->id.productID = wcstoul(wstr.c_str() + pos + 4, nullptr, 16);
            pos = wstr.find(L"vid_");
            gp->id.vendorID = wcstoul(wstr.c_str() + pos + 4, nullptr, 16);
            ++xinput_index;
        }
        else
            break;
}
#endif
#endif

    return gamepads;
}

#elif defined(__linux__) || defined(linux)// if(windows)

#include <linux/joystick.h>
#include <fcntl.h>
#include <unistd.h>
#include <libudev.h>
#include <dirent.h>

#include <string.h>

#if defined(GAMEPAD_ENABLE_XPAD) && defined(GAMEPAD_ENABLE_XBOXDRV)
    #error "You must choose a key mapping depending on the driver xpad or xboxdrv, you can't choose both"
#endif

/* Number of bits for 1 unsigned char */
#define nBitsPerUchar          (sizeof(unsigned char) * 8)

/* Number of unsigned chars to contain a given number of bits */
#define nUcharsForNBits(nBits) ((((nBits)-1)/nBitsPerUchar)+1)

/* Index=Offset of given bit in 1 unsigned char */
#define bitOffsetInUchar(bit)  ((bit)%nBitsPerUchar)

/* Index=Offset of the unsigned char associated to the bit
   at the given index=offset */
#define ucharIndexForBit(bit)  ((bit)/nBitsPerUchar)

/* Value of an unsigned char with bit set at given index=offset */
#define ucharValueForBit(bit)  (((unsigned char)(1))<<bitOffsetInUchar(bit))

#define testBit(bit, array)    ((array[ucharIndexForBit(bit)] >> bitOffsetInUchar(bit)) & 1)

#define NUM_EFFECTS (FF_EFFECT_MAX-FF_EFFECT_MIN+1)
#define EFFECT_INDEX(EFFECT_ID) (EFFECT_ID-FF_EFFECT_MIN)

class Linux_Gamepad : public Gamepad
{
    int _fd;
    int _event_fd;
    int _led_fd;

    struct ff_effect _effects[NUM_EFFECTS];

public:
    Linux_Gamepad(int fd, int event_fd, int led_fd) :
        _fd(fd),
        _event_fd(event_fd),
        _led_fd(led_fd)
    {
        for(int i = 0; i < NUM_EFFECTS; ++i)
            _effects[i].id = -1;

        get_available_effects();
    }

    virtual ~Linux_Gamepad()
    {
        for (int i = 0; i < NUM_EFFECTS; ++i)
            unregister_effect(_effects[i]);

        if (_fd != -1)
            close(_fd);
        if (_event_fd != -1)
            close(_event_fd);
        if (_led_fd != -1)
            close(_led_fd);
    }

    void get_available_effects()
    {
        if (_event_fd != -1)
        {
            unsigned char ffFeatures[1 + FF_MAX / 8 / sizeof(unsigned char)] = { 0 };
            if (ioctl(_event_fd, EVIOCGBIT(EV_FF, sizeof(ffFeatures) * sizeof(unsigned char)), ffFeatures) != -1)
            {
            }
        }
    }

    void register_effect(struct ff_effect &ff)
    {
        if (_event_fd == -1 || ioctl(_event_fd, EVIOCSFF, &ff) == -1)
        {
            //std::cout << "Failed to register effect " << ff.type << std::endl;
            ff.id = -1;
        }
    }

    void unregister_effect(struct ff_effect &ff)
    {
        if(ff.id == -1 || _event_fd == -1)
            return;

        stop_effect(ff);

        // delete the effect
        if (ioctl(_event_fd, EVIOCRMFF, ff.id) == -1)
        {
            //std::cout << "Failed to unregister effect " << ff.type << std::endl;
        }
        ff.id = -1;
    }

    bool play_effect(struct ff_effect &ff)
    {
        if(ff.id == -1 || _event_fd == -1)
            return false;

        struct input_event play;

        play.type = EV_FF;
        play.code = ff.id;
        play.value = 1;

        if (write(_event_fd, (const void*)&play, sizeof(play)) == -1)
        {
            //std::cout << "Failed to play effect " << effect_id << std::endl;
            return false;
        }

        return true;
    }

    bool stop_effect(struct ff_effect &ff)
    {
        if(ff.id == -1 || _event_fd == -1)
            return false;

        struct input_event play;

        play.type = EV_FF;
        play.code = ff.id;
        play.value = 0;

        if (write(_event_fd, (const void*)&play, sizeof(play)) == -1)
        {
            //std::cout << "Failed to stop effect " << effect_id << std::endl;
            return false;
        }

        return true;
    }

    virtual int GetXinputId()
    {
        return -1;
    }

    virtual bool RunFrame()
    {
        struct js_event je;
        bool r = false;
        while (read(_fd, &je, sizeof(je)) > 0)
        {
            r = true;
            switch (je.type)
            {
            case JS_EVENT_BUTTON:
                switch (je.number)
                {
                    case 0: a = je.value; break;
                    case 1: b = je.value; break;
                    case 2: x = je.value; break;
                    case 3: y = je.value; break;
                    case 4: left_shoulder = je.value; break;
                    case 5: right_shoulder = je.value; break;
                    case 6: back = je.value; break;
                    case 7: start = je.value; break;
                    case 8: guide = je.value; break;
                    case 9: left_thumb = je.value; break;
                    case 10: right_thumb = je.value; break;
                }
                break;

            case JS_EVENT_AXIS:
                switch (je.number)
                {
#if defined(GAMEPAD_ENABLE_XPAD)
                    case 0: left_stick.x = je.value / 32767.0f; break;
                    case 1: left_stick.y = je.value / -32767.0f; break;
                    case 2: left_trigger = (je.value + 32767) / 65534.0f; break;
                    case 3: right_stick.x = je.value / 32767.0f; break;
                    case 4: right_stick.y = je.value / -32767.0f; break;
                    case 5: right_trigger = (je.value + 32767) / 65534.0f; break;
#endif
#if defined(GAMEPAD_ENABLE_XBOXDRV)
                    case 0: left_stick.x = je.value / 32767.0f; break;
                    case 1: left_stick.y = je.value / -32767.0f; break;
                    case 2: right_stick.x = je.value / 32767.0f; break;
                    case 3: right_stick.y = je.value / -32767.0f; break;
                    case 4: right_trigger = (je.value + 32767) / 65534.0f; break;
                    case 5: left_trigger = (je.value + 32767) / 65534.0f; break;
#endif
                case 6:
                    left  = je.value == -32767;
                    right = je.value ==  32767;
                    break;
                case 7:
                    up   = je.value == -32767;
                    down = je.value ==  32767;
                    break;
                }
                break;
            }
        }
        return r;
    }

    virtual bool SetVibration(uint16_t left_speed, uint16_t right_speed)
    {
        if (_event_fd == -1)
          return false;
        
        struct ff_effect& rumble = _effects[EFFECT_INDEX(FF_RUMBLE)];

        if (rumble.u.rumble.strong_magnitude == left_speed &&
            rumble.u.rumble.weak_magnitude == right_speed)
            return true;

        if (rumble.id != -1)
        {
            stop_effect(rumble);
            unregister_effect(rumble);
            rumble.u.rumble.strong_magnitude = 0;
            rumble.u.rumble.weak_magnitude = 0;
        }

        if(left_speed == 0 && right_speed == 0)
            return true;

        rumble.type = FF_RUMBLE;
        rumble.id = -1;
        rumble.u.rumble.strong_magnitude = left_speed;
        rumble.u.rumble.weak_magnitude = right_speed;
        rumble.replay.length = 0xFFFF;
        rumble.replay.delay = 0;

        register_effect(rumble);
        return play_effect(rumble);
    }

    virtual bool SetLed(uint8_t r, uint8_t g, uint8_t b)
    {
        return false;
    }
};

std::vector<std::shared_ptr<Gamepad>> Gamepad::get_gamepads()
{
    std::vector<std::shared_ptr<Gamepad>> gamepads;

    DIR* input_dir;
    struct dirent *input_dir_entry;

    if ((input_dir = opendir("/sys/class/input")) == nullptr)
        return gamepads;

    while ((input_dir_entry = readdir(input_dir)) != nullptr)
    {
        if (strncmp(input_dir_entry->d_name, "js", 2) != 0)
            continue;

        std::string js_path("/dev/input/");
        js_path += input_dir_entry->d_name;

        int dev_fd = open(js_path.c_str(), O_RDWR | O_NONBLOCK);
        int event_fd = -1;
        int led_fd = -1;
        if (dev_fd == -1)
        {
            if (errno == EACCES)
                dev_fd = open(js_path.c_str(), O_RDONLY | O_NONBLOCK);
            if(dev_fd == -1)
                continue;
        }

        std::string device_path("/sys/class/input/");
        device_path += input_dir_entry->d_name;
        device_path += "/device";

        DIR *device_dir;
        struct dirent *device_dir_entry;

        if ((device_dir = opendir(device_path.c_str())) != nullptr)
        {// Open sys device directory to find the event associated (for rumble)
            while ((device_dir_entry = readdir(device_dir)) != nullptr)
            {
                if (device_dir_entry->d_type != DT_DIR || strncmp(device_dir_entry->d_name, "event", 5))
                    continue;

                device_path = "/dev/input/";
                device_path += device_dir_entry->d_name;
                event_fd = open(device_path.c_str(), O_RDWR);
                break;
            }
            closedir(device_dir);
        } 

        std::string led_path("/sys/class/leds/xpad");
        led_path += js_path.substr(js_path.rfind("/js")+3) + "/brightness";
        led_fd = open(led_path.c_str(), O_RDWR);

        std::shared_ptr<Gamepad> gp(new Linux_Gamepad(dev_fd, event_fd, led_fd));
        gamepads.emplace_back(gp);
    }

    closedir(input_dir);
    return gamepads;
}

#endif//elif(linux)
