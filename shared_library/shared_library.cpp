/* Copyright (C) 2019 Nemirtingas
 * This file is part of shared_library.
 *
 * shared_library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * shared_library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with shared_library.  If not, see <https://www.gnu.org/licenses/>
 */

#include <shared_library/shared_library.h>

shared_library::shared_library(): _module(nullptr)
{}

shared_library::~shared_library()
{
	unload();
}

void* shared_library::get_func_by_name( const char* funcName )
{
	if( loaded() )
	{
		void* funcAddr = load_function(funcName);
		if( !funcAddr )
			throw(std::exception());
			
		return funcAddr;
	}
	return 0;
}

module_t shared_library::load_library() const
{
    #if defined(__WINDOWS__)
        return LoadLibraryA(_filename.c_str());
    #elif defined(__LINUX__)
        return dlopen(_filename.c_str(), RTLD_LAZY);
    #endif
}

int shared_library::free_library() const
{
    #if defined(__WINDOWS__)
        return FreeLibrary(_module);
    #elif defined(__LINUX__)
        return dlclose(_module);
    #endif
}

void* shared_library::load_function( const char* funcName ) const
{
	#if defined(__WINDOWS__)
		return GetProcAddress(_module, funcName);
	#elif defined(__LINUX__)
		return dlsym(_module, funcName);
	#endif
}

bool shared_library::load( const char* filename )
{
	if( filename != nullptr && filename[0] != 0 )
	{
		if (!loaded())
		{
			_module = shared_library::load_library();
			_filename = filename;
		}
	}
	return loaded();
}

bool shared_library::load( const std::string &filename )
{
	if (!filename.empty())
	{
		if (!loaded())
		{
			_module = shared_library::load_library();
			_filename = filename;
		}
	}
	return loaded();
}

int shared_library::unload()
{
	int res = 0;
	if( loaded() )
	{
		res = free_library();
		_module = 0;
	}
	return res;
}

bool shared_library::loaded() const
{
	return (_module != 0) ? true : false;
}

shared_library::operator bool() const
{
	return loaded();
}

std::string const& shared_library::get_name() const
{
	return _filename;
}

module_t shared_library::get_native_handle() const
{
	return _module;
}
