/* Copyright (C) 2019 Nemirtingas
 * This file is part of shared_library.
 *
 * shared_library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * shared_library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with shared_library.  If not, see <https://www.gnu.org/licenses/>
 */

#ifndef __SHARED_LIBRARY_INCLUDED__
#define __SHARED_LIBRARY_INCLUDED__

#if defined(WIN32) || defined(_WIN32) || defined(__MINGW32__) ||\
    defined(WIN64) || defined(_WIN64) || defined(__MINGW64__)
	#include <Windows.h>
	typedef HMODULE module_t;
#elif defined(__linux__) || defined(linux)
	#include <dlfcn.h>
	typedef void* module_t;
#endif

#include <exception>
#include <string>

class shared_library
{
	module_t                     _module;
	std::string                  _filename;

	void*    get_func_by_name( const char* funcName );
	module_t load_library() const;
	int      free_library() const;
	void*    load_function( const char* funcName ) const;

	shared_library(shared_library const& other) = delete;
	shared_library(shared_library && other) = delete;
	shared_library& operator =(shared_library const& other) = delete;
	shared_library& operator =(shared_library && other) = delete;

public:
	shared_library();
	~shared_library();

	bool load( const char* filename );
	bool load( const std::string &filename );

	int unload();

	template<typename T>
	T function( const char* funcName )
	{
		return reinterpret_cast<T>(get_func_by_name(funcName));
	}

	template<typename T>
	T function( const std::string &funcName )
	{
		return reinterpret_cast<T>(get_func_by_name(funcName.c_str()));
	}

	bool                    loaded           () const;
	operator                bool             () const;
	std::string const&      get_name         () const;
	module_t                get_native_handle() const;
};

#endif // __SHARED_LIBRARY_INCLUDED__
