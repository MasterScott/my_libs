#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>

#define NBYTES 16

#define __UUID_PRINTF_FORMAT__ \
"%02" SCNx8 "%02" SCNx8 "%02" SCNx8 "%02" SCNx8 "-"\
"%02" SCNx8 "%02" SCNx8 "-"\
"%02" SCNx8 "%02" SCNx8 "-"\
"%02" SCNx8 "%02" SCNx8 "-"\
"%02" SCNx8 "%02" SCNx8 "%02" SCNx8 "%02" SCNx8 "%02" SCNx8 "%02" SCNx8

int main()
{
	//                                           12345678 9012 3456 7890 123456789012
	// Returns a 36-character string in the form XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
	// where "X" is an "upper-case" hexadecimal digit [0-9A-F].
	// Use the LCase function if you want lower-case letters.

	time_t s = time(nullptr);
	s ^= 0x51F9A20C;
	srand(s);
	srand(rand());

	uint8_t abData[NBYTES];
	char uuid[37] = { 0 };

	// 1. Generate 16 random bytes = 128 bits
	for (int i = 0; i < NBYTES; ++i)
	{
		abData[i] = static_cast<uint8_t>(rand());
	}

	// Adjust certain bits according to RFC 4122 section 4.4.
	// This just means do the following
	// (a) set the high nibble of the 7th byte equal to 4 and
	// (b) set the two most significant bits of the 9th byte to 10'B,
	//     so the high nibble will be one of {8,9,A,B}.
	abData[6] = 0x40 | (abData[6] & 0xf);
	abData[8] = 0x80 | (abData[8] & 0x3f);

	const uint8_t *datas = reinterpret_cast<const uint8_t*>(&abData);
	snprintf(uuid, 37, __UUID_PRINTF_FORMAT__,
		datas[3], datas[2], datas[1], datas[0],
		datas[5], datas[4],
		datas[7], datas[6],
		datas[8], datas[9],
		datas[10], datas[11], datas[12], datas[13], datas[14], datas[15]);

	// Return the UUID string
	printf("%s", uuid);

    return 0;
}

