#include "crc32_tables.inl"

enum class crc_generator
{
        crc32_normal               = 0,
        crc32_reversed             = 1,
        crc32_reversed_reciprocal  = 2,
        crc32c_normal              = 3,
        crc32c_reversed            = 4,
        crc32c_reversed_reciprocal = 5,
        crc32K_normal              = 6,
        crc32K_reversed            = 7,
        crc32K_reversed_reciprocal = 8,
        crc32Q_normal              = 9,
        crc32Q_reversed            = 10,
        crc32Q_reversed_reciprocal = 11,
};


uint32_t crc32( uint32_t crc, uint8_t c, crc_generator generator = crc_generator::crc32_normal )
{
        return (crc >> 8) ^ crc32_tables[(uint32_t)generator][(crc ^ c) & 0xFF];
}

uint32_t crc32( uint32_t crc, uint8_t const *c, uint32_t len, crc_generator generator = crc_generator::crc32_normal )
{
        while( len-- )
                crc = crc32(crc, *c++, generator);

        return crc;
}

uint32_t crc32( uint8_t const *c, uint32_t len, crc_generator generator = crc_generator::crc32_normal )
{
        return crc32(0xFFFFFFFF, c, len, generator);
}

